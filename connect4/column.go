package connect4

type column [6]pin

func (c *column) InsertNewPin(p pin) {
	for i, v := range c {
		if v == emptyPin {
			c[i] = p
			break
		}
	}
}

func (c column) CanInsertNewPin() bool {
	return c[len(c)-1] == emptyPin
}
