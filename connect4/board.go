package connect4

import (
	"fmt"
	"strconv"
	"strings"
)

func takeWhileFirst(x []pin) ([]pin, []pin) {
	var i int = 0

	for i = 0; i < len(x); i++ {
		if x[i] != x[0] {
			break
		}
	}

	if i == len(x) {
		return x, nil
	}
	return x[0:i], x[i:]
}

func winnerInSlice(col []pin) pin {
	for len(col) > 0 {
		head, tail := takeWhileFirst(col)
		if len(head) >= 4 {
			return head[0]
		}
		col = tail
	}
	return emptyPin
}

type board [7]column

func (b board) String() string {

	var str strings.Builder

	// Make first row on column numbers
	str.WriteString("\t")
	for i := 0; i < 7; i++ {
		str.WriteString("   " + strconv.Itoa(i+1) + "   ")
	}
	str.WriteString("\n")

	// Print columns
	for i := 5; i >= 0; i-- {
		tmp := make([]string, 0, 7)
		for j := 0; j < 7; j++ {
			tmp = append(tmp, fmt.Sprintf("|  %s   ", b[j][i]))
		}
		str.WriteString("\t" + strings.Join(tmp, "") + "|\n")
	}

	// Add final row
	str.WriteString("\t")
	for i := 0; i <= 7*7; i++ {
		str.WriteString("-")
	}
	str.WriteString("\n")

	return str.String()
}

func (b *board) ResetBoard() error {
	if b != nil {
		for j := 0; j < 7; j++ {
			for i := 0; i < 6; i++ {
				b[j][i] = emptyPin
			}
		}
		return nil
	}
	return fmt.Errorf("Nil pointer to board, trying to reset an empty board")
}

func (b *board) InsertInColumn(p pin, colIndex int) error {
	if b[colIndex].CanInsertNewPin() {
		b[colIndex].InsertNewPin(p)
		return nil
	}
	return fmt.Errorf("Cannot insert pin in column %d", colIndex+1)
}

func (b board) IsThereAWinner() pin {
	pinCol := b.checkWinnerInColumns()
	pinRow := b.checkWinnerInRows()
	pinDiag := b.checkWinnerInDiagonals()

	switch pinCol | pinRow | pinDiag {
	case player1Pin:
		return player1Pin
	case player2Pin:
		return player2Pin
	default:
		return emptyPin
	}
}

func (b board) IsItADraw() bool {
	cond1, cond2 := (b.IsThereAWinner() == emptyPin), false
	for i := 0; i < 7; i++ {
		cond2 = cond2 || b[i].CanInsertNewPin()
	}
	return cond1 && !cond2
}

func (b board) checkWinnerInColumns() pin {
	result := map[pin]bool{player1Pin: false, player2Pin: false}

	for i := 0; i < 7; i++ {
		tmp := make([]pin, 0, 6)

		for j := 0; j < 6; j++ {
			tmp = append(tmp, b[i][j])
		}

		candidate := winnerInSlice(tmp)

		switch candidate {
		case player1Pin:
			result[player1Pin] = true
		case player2Pin:
			result[player2Pin] = true
		}
	}

	if result[player1Pin] {
		return player1Pin
	}
	if result[player2Pin] {
		return player2Pin
	}
	return emptyPin
}

func (b board) checkWinnerInRows() pin {
	result := map[pin]bool{player1Pin: false, player2Pin: false}

	for i := 0; i < 6; i++ {
		tmp := make([]pin, 0, 7)

		for j := 0; j < 7; j++ {
			tmp = append(tmp, b[j][i])
		}

		candidate := winnerInSlice(tmp)

		switch candidate {
		case player1Pin:
			result[player1Pin] = true
		case player2Pin:
			result[player2Pin] = true
		}
	}

	if result[player1Pin] {
		return player1Pin
	}
	if result[player2Pin] {
		return player2Pin
	}
	return emptyPin
}

func (b board) checkWinnerInDiagonals() pin {
	result := map[pin]bool{player1Pin: false, player2Pin: false}

	for k := 3; k < 9; k++ {
		tmp1, tmp2 := make([]pin, 0, 7), make([]pin, 0, 7)

		for icol := 0; icol < 7; icol++ {
			for irow := 0; irow < 6; irow++ {

				if icol+irow == k {
					tmp1 = append(tmp1, b[icol][irow])
				}

				if irow-icol == k-6 {
					tmp2 = append(tmp2, b[icol][irow])
				}
			}
		}

		candidate1, candidate2 := winnerInSlice(tmp1), winnerInSlice(tmp2)

		switch candidate1 | candidate2 {
		case player1Pin:
			result[player1Pin] = true
		case player2Pin:
			result[player2Pin] = true
		}
	}

	if result[player1Pin] {
		return player1Pin
	}
	if result[player2Pin] {
		return player2Pin
	}
	return emptyPin
}
