package connect4

type pin int

const (
	emptyPin pin = iota
	player1Pin
	player2Pin
)

func (p pin) String() string {
	switch p {
	case emptyPin:
		return "\u25ef"
	case player1Pin:
		return "\033[31m\u2b24\033[0m"
	case player2Pin:
		return "\033[33m\u2b24\033[0m"
	default:
		return ""
	}
}
