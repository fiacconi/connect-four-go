package connect4

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func clearShell() {
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()
}

func readInput() (int, error) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("[Insert a column number between 1 and 7]: ")
	raw, err := reader.ReadString('\n')
	if err != nil {
		return 0, err
	}

	colNumber, err := strconv.Atoi(strings.TrimRight(raw, "\r\n "))
	if err != nil {
		return 0, fmt.Errorf("Cannot convert raw input column in int")
	}

	if (colNumber < 1) || (colNumber > 7) {
		return 0, fmt.Errorf("Input columns must be in interval [1,7]")
	}

	return colNumber - 1, nil
}

func playOneMoreMatch() (bool, error) {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Do you want to play another match? [y/n]: ")
	ans, err := reader.ReadString('\n')
	if err != nil {
		return false, err
	}
	ans = strings.ToLower(strings.TrimRight(ans, "\r\n "))

	for (ans != "n") && (ans != "y") {
		fmt.Print("\nPlease answer y or n\n\n")
		fmt.Print("Do you want to play another match? [y/n]: ")
		ans, err = reader.ReadString('\n')
		if err != nil {
			return false, err
		}
		ans = strings.ToLower(strings.TrimRight(ans, "\r\n "))
	}

	if ans == "y" {
		return true, nil
	}
	return false, nil
}

// Game defines the type object that describe a connect 4 game.
type Game struct {
	gameBoard     board
	currentPlayer pin
}

func (g *Game) nextPlayer() {
	if g.currentPlayer == player1Pin {
		g.currentPlayer = player2Pin
	} else {
		g.currentPlayer = player1Pin
	}
}

func (g *Game) gameStep() {
	fmt.Printf("\n%s\n", g.gameBoard)
	fmt.Printf("\nPlayer %s  moves\n\n", g.currentPlayer)

	colNum, err := readInput()
	if err != nil {
		g.gameStep()
	} else {
		if err := g.gameBoard.InsertInColumn(g.currentPlayer, colNum); err != nil {
			g.gameStep()
		}
	}
}

// GameLoop runs the main loop of the game
func (g *Game) GameLoop() {

	g.currentPlayer = player1Pin

	clearShell()

	for {
		if g.gameBoard.IsThereAWinner() != emptyPin {
			fmt.Printf("\n%s\n", g.gameBoard)
			fmt.Printf("\nPLAYER %s  WON !!!\n\n", g.currentPlayer)

			if playMore, err := playOneMoreMatch(); playMore && (err == nil) {
				g.gameBoard.ResetBoard()
				clearShell()
				continue
			} else {
				return
			}
		} else if g.gameBoard.IsItADraw() {
			fmt.Printf("\n%s\n", g.gameBoard)
			fmt.Println("\nDRAW!")
			return
		} else {
			g.nextPlayer()
			g.gameStep()
		}
	}
}
