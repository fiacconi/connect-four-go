package main

import (
	"connect4/connect4"
)

func main() {
	game := connect4.Game{}
	game.GameLoop()
}
